from app import app, db
from flask import render_template, flash, redirect, url_for, request
from app.forms import LoginForm, RegistrationForm, CreateSubreddit, CreatePost, EditProfileForm, CommentForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Post, Subreddit, Comment
from werkzeug.urls import url_parse

@app.route('/')
@app.route('/index')
def index():
    subreddits = Subreddit.query.all()
    if not current_user.is_authenticated:
        posts = Post.query.all()
    else:
        posts = current_user.followed_posts().all()
    return render_template('index.html', posts=posts, subreddits=subreddits)




@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/users/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(user_id=current_user.id)
    return render_template('user.html', user=user, posts=posts)


@app.route('/create_subreddit', methods=['GET', 'POST'])
@login_required
def create_subreddit():
    form = CreateSubreddit()
    if form.validate_on_submit():
        name = Subreddit(subreddit_name=form.subreddit_name.data, user_id=current_user.id)
        db.session.add(name)
        db.session.commit()
        return redirect(url_for('index'))
    print("last",form.errors)
    return render_template('create_subreddit.html', form=form)

@app.route('/subreddits/<subreddit>', methods=['GET', 'POST'])
@login_required
def subreddit(subreddit):
    # subreddit_id = Subreddit.query.filter_by(subreddit_name=subreddit).first()
    print(subreddit)
    sub = Subreddit.query.filter_by(subreddit_name=subreddit).first()
    sub_id = sub.id
    followers = sub.followers.count()
    posts = Post.query.filter_by(subreddit_id=sub_id)
    return render_template('subreddit.html', posts=posts, sub=sub, followers=followers)

@app.route('/create_post', methods=['GET', 'POST'])
@login_required
def create_post():
    form = CreatePost()
    if form.validate_on_submit():
        post = Post(title=form.title.data, body=form.body.data, subreddit_id=form.subreddit_id.data, user_id=current_user.id)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('create_post.html', form=form)

@app.route('/like/<int:post_id>/<action>')
@login_required
def like_action(post_id, action):
    post = Post.query.filter_by(id=post_id).first_or_404()
    if action == 'like':
        current_user.like_post(post)
        db.session.commit()
    if action == 'unlike':
        current_user.unlike_post(post)
        db.session.commit()
    return redirect(request.referrer)

@app.route('/follow/<subreddit>')
@login_required
def follow(subreddit):
    subreddit_name = Subreddit.query.filter_by(subreddit_name=subreddit).first()
    if subreddit_name is None:
        flash('Subreddit {} not found.'.format(subreddit_name))
        return redirect(url_for('index'))
    
    current_user.follow(subreddit_name)
    db.session.commit()
    flash('You are following {}!'.format(subreddit))
    return redirect(url_for('index'))

@app.route('/unfollow/<subreddit>')
@login_required
def unfollow(subreddit):
    subreddit_name = Subreddit.query.filter_by(subreddit_name=subreddit).first()
    if subreddit_name is None:
        flash('Subreddit {} not found.'.format(subreddit_name))
        return redirect(url_for('index'))
    current_user.unfollow(subreddit_name)
    db.session.commit()
    flash('You are not following {}.'.format(subreddit))
    return redirect(url_for('index'))


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)

@app.route('/comments/<int:post_id>', methods=['GET', 'POST'])
@login_required
def comments(post_id):
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(user_id = current_user.id, post_id=post_id, body=form.comment.data)
        db.session.add(comment)
        db.session.commit()
    return redirect(request.referrer)

@app.route('/posts/<int:post_id>', methods=['GET', 'POST'])
@login_required
def posts(post_id):
    form = CommentForm()
    post = Post.query.filter_by(id=post_id).first()
    comments = Comment.query.filter_by(post_id=post_id).all()
    return render_template('post_page.html', post=post, comments=comments, form=form)
