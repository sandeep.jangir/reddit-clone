from app import db, login
from datetime import datetime
from werkzeug import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app.search import *

# class SearchableMixin(object):
#     @classmethod
#     def search(cls, expression, page, per_page):
#         ids, total = query_index(cls.__tablename__, expression, page, per_page)
#         if total == 0:
#             return cls.query.filter_by(id=0), 0
#         when = []
#         for i in range(len(ids)):
#             when.append((ids[i], i))
#         return cls.query.filter(cls.id.in_(ids)).order_by(
#             db.case(when, value=cls.id)), total

#     @classmethod
#     def before_commit(cls, session):
#         session._changes = {
#             'add': list(session.new),
#             'update': list(session.dirty),
#             'delete': list(session.deleted)
#         }

#     @classmethod
#     def after_commit(cls, session):
#         for obj in session._changes['add']:
#             if isinstance(obj, SearchableMixin):
#                 add_to_index(obj.__tablename__, obj)
#         for obj in session._changes['update']:
#             if isinstance(obj, SearchableMixin):
#                 add_to_index(obj.__tablename__, obj)
#         for obj in session._changes['delete']:
#             if isinstance(obj, SearchableMixin):
#                 remove_from_index(obj.__tablename__, obj)
#         session._changes = None

#     @classmethod
#     def reindex(cls):
#         for obj in cls.query:
#             add_to_index(cls.__tablename__, obj)

# db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
# db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('subreddits.id'))
)

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    about_me = db.Column(db.String(140))
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    subreddit = db.relationship('Subreddit', backref='creator', lazy='dynamic')

    followed = db.relationship('Subreddit', secondary=followers, backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')

    voted = db.relationship('PostProperties', foreign_keys='PostProperties.user_id',
        backref='user', lazy='dynamic')

    comments = db.relationship('Comment', foreign_keys='Comment.user_id', backref='user', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def is_following(self, subreddit):
        return self.followed.filter(followers.c.followed_id == subreddit.id).count() > 0

    def follow(self, subreddit):
        if not self.is_following(subreddit):
            self.followed.append(subreddit)

    def unfollow(self, subreddit):
        if self.is_following(subreddit):
            self.followed.remove(subreddit)


    def followed_posts(self):
        followed = Post.query.join(followers, (followers.c.followed_id == Post.subreddit_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)     
        return followed.union(own).order_by(Post.timestamp.desc())


    def has_liked_post(self, post):
        return PostProperties.query.filter(PostProperties.user_id == self.id, PostProperties.post_id == post.id).count() > 0

    def like_post(self, post):
        if not self.has_liked_post(post):
            like = PostProperties(user_id=self.id, post_id=post.id)
            db.session.add(like)


    def unlike_post(self, post):
        if self.has_liked_post(post):
            PostProperties.query.filter_by(user_id=self.id, post_id=post.id).delete()

class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    __searchable__ = ['body']
    title = db.Column(db.String(20))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    subreddit_id = db.Column(db.Integer, db.ForeignKey('subreddits.id'))
    
    likes = db.relationship('PostProperties', backref='post', lazy='dynamic')

    comments = db.relationship('Comment', foreign_keys='Comment.post_id', backref='parent_post', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.title)
    
class Subreddit(db.Model):
    __tablename__ = 'subreddits'
    id = db.Column(db.Integer, primary_key=True)
    subreddit_name = db.Column(db.String(64), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    posts = db.relationship('Post', backref='subreddit', lazy='dynamic')

    def __repr__(self):
        return '<subreddit {}>'.format(self.subreddit_name)

class PostProperties(db.Model):
    __tablename__ = 'post_properties'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))

class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)