from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_moment import Moment
from elasticsearch import Elasticsearch
from flask_bootstrap import Bootstrap
import logging
import os
from logging.handlers import SMTPHandler, RotatingFileHandler

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
moment = Moment(app)
bootstrap = Bootstrap(app)
elasticsearch = Elasticsearch('http://localhost:9200')



if app.config['LOG_TO_STDOUT']:
   stream_handler = logging.StreamHandler()
   stream_handler.setLevel(logging.INFO)
   app.logger.addHandler(stream_handler)
else:
   if not os.path.exists('logs'):
       os.mkdir('logs')
   file_handler = RotatingFileHandler('logs/reddit.log',
                                       maxBytes=10240, backupCount=10)
   file_handler.setFormatter(logging.Formatter(
       '%(asctime)s %(levelname)s: %(message)s '
       '[in %(pathname)s:%(lineno)d]'))
   file_handler.setLevel(logging.INFO)
   app.logger.addHandler(file_handler)

app.logger.setLevel(logging.INFO)
app.logger.info('Reddit startup')


from app import routes, models

app.run(host='0.0.0.0', port=80)