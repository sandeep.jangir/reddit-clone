FROM python:3.6-alpine
RUN adduser -D reddit
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
WORKDIR /home/reddit
COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install psycopg2-binary
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn
COPY app app
COPY migrations migrations
COPY reddit.py config.py boot.sh ./
RUN chmod +x boot.sh
ENV FLASK_APP reddit.py
RUN chown -R reddit:reddit ./
USER reddit
EXPOSE 5000
ENTRYPOINT ["./boot.sh"]